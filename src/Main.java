import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class Main {

    public static void main(String argv[]) {
        try {
            HtmlCleaner htmlCleaner = new HtmlCleaner();
            TagNode tagNode = htmlCleaner.clean(new URL("http://ad1954.blog126.fc2.com/blog-entry-548.html"));

            TagNode titleTagNodeArray[] = tagNode.getElementsByName("title", true);
            for(TagNode titleTagNode: titleTagNodeArray) {
                System.out.println("title: " + titleTagNode.getText().toString());
            }

            TagNode contentTagNodeArray[] = tagNode.getElementsByAttValue("class", "entry_body", true, false);
            for(TagNode contentTagNode: contentTagNodeArray) {
                System.out.println("content: " + contentTagNode.getText().toString());
            }

            TagNode timeTagNodeArray[] = tagNode.getElementsByAttValue("title","記事URL", true, false);
            for(TagNode timeTagNode: timeTagNodeArray) {
                System.out.println("time: " + timeTagNode.getText().toString());
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
